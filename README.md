GMLog V2
========

GMLog V2 is a remote logging application, geared towards GameMaker Studio.
It aims to provide simple logging capability to GameMaker Studio.

The project is released under the Terms of the General Public License 3 with the exceptions of the scripts provided for GameMaker Studio. Those scripts, residing in the "scripts" folder within the project, have been placed within the Public Domain and can be used freely.
This enables professional developers to use the tool and the scripts in their project without having to provide the source for their games. Using the Tool itself does not require you to do so.
