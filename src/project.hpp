/*   
	GMLog V2 - A remote Logging Tool for Game Maker Studio
    Copyright (C) 2013  Scorcher24

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.
*/
#ifndef PROJECT_HPP
#define PROJECT_HPP

/////////////////////////////////////////////
// wxWidgets
/////////////////////////////////////////////
#include <wx/wx.h>
#include <wx/socket.h>
#include <wx/textctrl.h>
#include <wx/notebook.h>
#include <wx/listctrl.h>
#include <wx/xml/xml.h>
#include <wx/sstream.h>



#endif 