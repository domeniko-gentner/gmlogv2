/*   
	GMLog V2 - A remote Logging Tool for Game Maker Studio
    Copyright (C) 2013  Scorcher24

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.
*/
#ifndef MYLISTVIEW_HPP
#define MYLISTVIEW_HPP

class Mainframe;

class MyListView : public wxListView
{
public:
	MyListView( wxWindow* parent, int id, MyListView* server, wxSocketBase* socket = NULL );
	virtual ~MyListView();

	// Convinience
	void AddError( wxString msg );
	void AddMessage( wxString msg );
	void AddWarning( wxString msg );

	// Events
	void OnSize( wxSizeEvent& );
	void OnSocket( wxSocketEvent& );

	// Network
	void ReadLogInput();

	// Decoder
	void ReadXml(const wxString& xml);

private:
	wxSocketBase* m_socket;
	unsigned char m_lastcode;
	MyListView* m_serverLog;
	DECLARE_EVENT_TABLE();
};

#endif