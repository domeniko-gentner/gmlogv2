/*   
	GMLog V2 - A remote Logging Tool for Game Maker Studio
    Copyright (C) 2013  Scorcher24

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.
*/
#include "project.hpp"
#include "Application.hpp"
#include "Mainframe.hpp"

bool Application::OnInit()
{
	Mainframe* f = new Mainframe( wxSize(800, 600), wxDefaultPosition );
	f->Centre();
	f->Show();
	
	SetTopWindow(f);
	return true;
}

IMPLEMENT_APP(Application);