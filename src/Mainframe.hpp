/*   
	GMLog V2 - A remote Logging Tool for Game Maker Studio
    Copyright (C) 2013  Scorcher24

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.
*/
#ifndef MAINFRAME_HPP
#define MAINFRAME_HPP

enum WindowIDs
{
	ID_SERVERLOG = wxID_HIGHEST + 1,
	ID_NOTEBOOK,
	ID_CLEAR_NOTEBOOK,
};

class MyListView;

class Mainframe : public wxFrame
{
public:
	Mainframe( const wxSize& size, const wxPoint& pos );
	virtual ~Mainframe();

	////////////////////////////////
	// Server Functions
	////////////////////////////////
	void StartServer();
	void StopServer();
	void Accept();

	////////////////////////////////
	// Events
	////////////////////////////////
	void SocketEvent( wxSocketEvent& );
	void OnQuit( wxCommandEvent& );
	void OnClearNotebook( wxCommandEvent& );

private:
	// Server Stuff
	wxSocketServer* m_listeningSocket;
	int m_port;

	// GUI Stuff
	MyListView* m_serverLog;
	wxNotebook* m_notebook;

	// Event Table
	DECLARE_EVENT_TABLE();
};

#endif 
