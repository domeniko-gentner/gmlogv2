/*   
	GMLog V2 - A remote Logging Tool for Game Maker Studio
    Copyright (C) 2013  Scorcher24

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.
*/
#include "project.hpp"
#include "MyListView.hpp"
#include "Mainframe.hpp"

BEGIN_EVENT_TABLE( Mainframe, wxFrame )
	EVT_SOCKET( wxID_ANY, Mainframe::SocketEvent )
	EVT_MENU( wxID_EXIT, Mainframe::OnQuit )
	EVT_MENU( ID_CLEAR_NOTEBOOK, Mainframe::OnClearNotebook )
END_EVENT_TABLE()

Mainframe::Mainframe(const wxSize& size, const wxPoint& pos)
	: wxFrame( NULL, -1, "GMLog V2", pos, size ), m_port(5000)
{	
	// Create Notebook
	m_notebook = new wxNotebook( this, ID_NOTEBOOK, wxDefaultPosition, wxDefaultSize, wxNB_TOP );

	// Add Server Log
	m_serverLog = new MyListView( m_notebook, ID_SERVERLOG, m_serverLog, NULL );	

	// Add Server Log to Notebook
	m_notebook->AddPage( m_serverLog, "Server Log", true );

	// Menu
	wxMenuBar* bar = new wxMenuBar();

	// Menu File
	wxMenu* mnuFile = new wxMenu();
	mnuFile->Append( wxID_EXIT, "&Quit\tCtrl-Q", "Exit GMLog", false );

	// Menu Edit
	wxMenu* mnuEdit = new wxMenu();
	mnuEdit->Append( ID_CLEAR_NOTEBOOK, "&Clear Tabs&\tF5", false );

	// Menu Help
	wxMenu* mnuHelp = new wxMenu();
	mnuHelp->Append(wxID_HELP, "&Help\tF1");
	mnuHelp->Append( wxID_ABOUT, "&About\tShift-F1");

	// Add Menus to the bar
	bar->Append( mnuFile, "&File" );
	bar->Append( mnuEdit, "&Edit" );
	bar->Append( mnuHelp, "&?"    );

	// Add to Frame
	this->SetMenuBar( bar );

	// Done
	m_serverLog->AddMessage("Welcome to GMLog V2 by Scorcher24 (http://nightlight2d.de)");
	m_serverLog->AddMessage("Visit https://github.com/scorcher24/gmlogv2 for news");
	m_serverLog->AddMessage("This program is licensed under the terms and conditions of the GNU General Public License V3");

	// Finally start the server
	StartServer();
}

Mainframe::~Mainframe()
{
	StopServer();
}

void Mainframe::OnClearNotebook( wxCommandEvent& e )
{	
	// Needs to be deleted backwards, 
	// otherwise we get a lot of out of index errors.
	while ( m_notebook->GetPageCount() > 0 )
	{
		int end = m_notebook->GetPageCount() - 1;
		if ( m_notebook->GetPageText( end ) != "Server Log" )
		{
			m_notebook->DeletePage(end);
		}
		else
		{
			break;
		}
	}
}

void Mainframe::OnQuit( wxCommandEvent& e )
{
	this->Close( true );
}

void Mainframe::StopServer()
{
	// Stop Server
	m_listeningSocket->Destroy();
	m_listeningSocket = NULL;
}

void Mainframe::StartServer()
{
	// Listening address
	wxIPV4address addr;
	addr.Service(m_port);

	// Creating Server
	m_listeningSocket = new wxSocketServer( addr );
	m_listeningSocket->SetEventHandler( *this );
	m_listeningSocket->SetNotify(wxSOCKET_CONNECTION_FLAG);
    m_listeningSocket->Notify(true);
    if (!m_listeningSocket->IsOk())
    {
		if (m_listeningSocket->LastError() == wxSOCKET_INVPORT){
               wxMessageBox("Port already in use!", "Server Error", wxOK|wxICON_ERROR, this);
			   m_serverLog->AddError("Port already in use!\n");
			   return;
		}

        m_serverLog->AddError("Cannot bind listening socket.");
		return;
    }
	m_serverLog->AddMessage("Server ready for input..");
}

void Mainframe::Accept()
{		
	// Accept connection
	wxSocketBase* incoming = m_listeningSocket->Accept();

	// Successful?
	if (!incoming){
		m_serverLog->AddError("Could not accept connection!");
	}	

	// Create new notebook tab and give socket to listview
	MyListView* l = new MyListView( m_notebook, -1, m_serverLog, incoming );
	m_notebook->AddPage( l, "Log", true );	
}

void Mainframe::SocketEvent( wxSocketEvent& evt )
{
    switch(evt.GetSocketEvent())
    {
        case wxSOCKET_CONNECTION:
			m_serverLog->AddMessage("Incoming Connection!\n");
			Accept();			
            break;

		default:
			m_serverLog->AddError("Error: Unhandled wxSocketEvent!");
			break;
    }
}

