/*   
	GMLog V2 - A remote Logging Tool for Game Maker Studio
    Copyright (C) 2013  Scorcher24

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.
*/
#include "project.hpp"
#include "MyListView.hpp"

BEGIN_EVENT_TABLE( MyListView, wxListView )
	EVT_SIZE( MyListView::OnSize )
	EVT_SOCKET( wxID_ANY, MyListView::OnSocket )
END_EVENT_TABLE()


MyListView::MyListView( wxWindow* parent, int id, MyListView* server, wxSocketBase* s )
	: wxListView( parent, id, wxDefaultPosition, wxDefaultSize, wxLC_REPORT|wxLC_NO_HEADER ), 
	  m_socket(s), m_lastcode(0), m_serverLog(server)
{
	// Insert one and only column
	this->InsertColumn( 0, "Entries");

	// Set Options for Socket
	if ( m_socket != NULL )
	{
		wxIPV4address addr;

		m_socket->SetEventHandler(*this);
		m_socket->SetNotify( wxSOCKET_INPUT_FLAG|wxSOCKET_LOST_FLAG );
		m_socket->SetFlags( wxSOCKET_NOWAIT );
		m_socket->Notify(true);

		if (!m_socket->GetPeer(addr) )
		{
			m_serverLog->AddError("Cannot get Peer Adress!");
		}
		else
		{
			m_serverLog->AddMessage("Client with IP4 Adress '" + addr.IPAddress() + "' connected\n");
		}
	}
}

MyListView::~MyListView()
{
	if ( m_socket != NULL ){
		delete m_socket;
	}
}

void MyListView::ReadXml( const wxString& xml )
{
	wxStringInputStream ss( xml );
	wxXmlDocument doc;

	if ( !doc.Load( ss ) ){
		m_serverLog->AddError("Received truncated or invalid xml transmission!: " + xml);
		return;
	}
	if ( !doc.IsOk() ){
		m_serverLog->AddError("Received truncated or invalid xml transmission (2)!:" + xml);
		return;
	}

	wxXmlNode* node = doc.GetRoot()->GetChildren();

	if ( node == NULL ){
		m_serverLog->AddError(" Received xml transmission without any children!");
	}

	while ( node != NULL )
	{
		if ( node->GetName() == "message" )
		{
			wxString type = node->GetAttribute("type");
			wxString msg  = node->GetAttribute("msg");

			if ( type == "error" )
			{
				AddError( msg );
			}
			else if ( type == "warning" )
			{
				AddWarning( msg );
			}
			else if ( type == "informal" )
			{
				AddMessage( msg );
			}
		}
		if ( node->GetName() == "control" )
		{
			wxString type = node->GetAttribute("type");
			wxString msg  = node->GetAttribute("msg");

			if ( type == "tabName" )
			{
				wxNotebook* book = (wxNotebook*)this->GetParent();
				if ( book != NULL )
				{
					int id = book->FindPage( this ); 
					if ( id != wxNOT_FOUND )
					{
						book->SetPageText( id, msg );
					}
				}
			}
		}
		node = node->GetNext();
		if ( node == NULL ){
			return;
		}
	}
}

void MyListView::ReadLogInput()
{
	wxString msg;
	while ( m_socket->IsData() )
	{
		char buff;
		m_socket->Read( &buff, 1 );
		if ( buff == '\0' ){
			break;
		}
		else{
			msg += buff;
		}
	}
	if ( !msg.IsEmpty() ){
		ReadXml( msg );
	}
}

void MyListView::OnSocket( wxSocketEvent& e )
{
	if ( m_socket != NULL )
	{
		switch( e.GetSocketEvent() )
		{
		case wxSOCKET_INPUT:
			{
				this->ReadLogInput();
				break;
			}
		case wxSOCKET_LOST:
			{
				this->AddError("Lost Connection to Client!");
				delete m_socket;
				m_socket = NULL;
				break;
			}
		}
	}
}

void MyListView::OnSize( wxSizeEvent& e )
{
	this->SetColumnWidth( 0, e.GetSize().GetWidth() - 1 );
}

void MyListView::AddError( wxString msg )
{
	int id = this->InsertItem( this->GetItemCount(), msg);
	this->SetItemBackgroundColour( id, wxColor(210, 49, 93));
}

void MyListView::AddMessage( wxString msg )
{
	int id = this->InsertItem( this->GetItemCount(), msg);
	this->SetItemBackgroundColour( id, wxColor(136, 193, 52));
}

void MyListView::AddWarning( wxString msg )
{
	int id = this->InsertItem( this->GetItemCount(), msg);
	this->SetItemBackgroundColour( id, wxColor(247, 200, 8));
}
