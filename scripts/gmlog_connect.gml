/**
    This file has been placed in the Public Domain.
**/
/**
    argument0 : Variable that holds the socket
    argument1 : IP
    argument2 : Port
**/

if ( GMLOG_ENABLED == 1 )
{
    var sock;
    // Create Socket to server
    sock = network_create_socket(network_socket_tcp);
    
    if (!sock){
        show_debug_message("Cannot create a socket for some reason!");
    }
    
    // Set timeouts
    network_set_timeout( sock, 20, 20 );
    
    // Connect raw since it is not a GM:S Game.
    network_connect_raw( sock, argument0, argument1 );
    return sock;
}
