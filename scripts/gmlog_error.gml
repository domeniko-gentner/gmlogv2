/**
    Notice: This file has been placed in the Public Domain.
**/
/// argument0 = socket, argument1 = message string

if ( GMLOG_ENABLED == 1 )
{
    var xml = '<?xml version="1.0" ?><gmlog><message type="error" msg="' + argument1 + '" /></gmlog>';   
    var msg_buffer = buffer_create( 256, buffer_grow, 2);
    
    buffer_write( msg_buffer, buffer_string, xml);
    
    network_send_raw( argument0, msg_buffer, buffer_get_size(msg_buffer) );
    
    buffer_delete(msg_buffer);
}
